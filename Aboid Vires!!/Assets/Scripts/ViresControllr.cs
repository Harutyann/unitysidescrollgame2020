﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViresControllr : MonoBehaviour
{
    
    GameObject player;
    GameObject director;
   
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("player");
        director = GameObject.Find("GameDirector");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.05f, 0, 0);

        if(transform.position.x<-9)
        {
            Destroy(gameObject);
        
        }
        Vector2 p1 = transform.position;
        Vector2 p2 = player.transform.position;
        Vector2 dir = p1 - p2;
        float d = dir.magnitude;
        float r1 = 0.5f;
        float r2 = 1.5f;

        if (d < r1 +r2)
        {
            director.GetComponent<GameDirector>().oto2 = true;
            
            director.GetComponent<GameDirector>().DecreaseHp();
            Destroy(gameObject);

        }
    }

    
}
