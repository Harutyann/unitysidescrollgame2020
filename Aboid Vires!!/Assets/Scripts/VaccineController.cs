﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VaccineController : MonoBehaviour
{
    public AudioClip PowerupSE;
    GameObject player;
    GameObject pd;
    public int count;
    GameObject se;
    

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("player");

        pd = GameObject.Find("PointController");

        se = GameObject.Find("PlayerController");
    }

    // Update is called once per frame
    void Update()
    {
      

        transform.Translate(-0.06f, 0, 0);

        if (transform.position.x < -9)
        {
            Destroy(gameObject);

        }
        Vector2 p1 = transform.position;
        Vector2 p2 = player.transform.position;
        Vector2 dir = p1 - p2;
        float d = dir.magnitude;
        float r1 = 0.5f;
        float r2 = 1.5f;

        if (d < r1 + r2)

        {
            //count ++;
            //Debug.Log(count);

            player.GetComponent<PlayerController>().oto1 = true;
            pd.GetComponent<PointDirector>().point += 100;
            //GameObject director = GameObject.Find("PointController");
            //director.GetComponent<PointDerector>().AddPoint();
            Destroy(gameObject);

            
        }
    }
}
