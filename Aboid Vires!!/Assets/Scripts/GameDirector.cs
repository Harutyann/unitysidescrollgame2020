﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    GameObject hpGauge;
    int hpflag = 10;
    public AudioClip SE;
    public bool oto2;
    // Start is called before the first frame update
   

   
   
    void Start()
    {
        hpGauge = GameObject.Find("hpGauge");
      
    }

    void Update()
    {
        if(oto2==true)
        {
            GetComponent<AudioSource>().Play();
            oto2 = false;
        }
    }

    public void DecreaseHp()
    {
        hpGauge.GetComponent<Image>().fillAmount -= 0.1f;
        hpflag -= 1;
        GetComponent<AudioSource>().Play();
        if(hpflag==0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }
}
