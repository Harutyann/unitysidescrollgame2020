﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaccineGenerator : MonoBehaviour
{
    public GameObject VaccinePrefab;
    float span = 1.0f;
    float delta = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        delta += Time.deltaTime;
        if (delta > span)
        {
            delta = 0;
            GameObject jo = Instantiate(VaccinePrefab) as GameObject;
            int px = Random.Range(-2, 5);
            jo.transform.position = new Vector3(8, px, 0);
        }
    }
}
