﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPointSound2 : MonoBehaviour

{
    public AudioClip sound;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "VaccinePrefab")
        {

            AudioSource.PlayClipAtPoint(sound, transform.position);

        }
    }
}
